module.exports = function (gulp, plugins, paths) {
    return function () {
        return gulp.src([
                paths.output.manifest.output_dir + '**/*.css',
                paths.output.manifest.output_dir + '**/*.js'
            ])
            .pipe(plugins.clean());
    };
};