module.exports = function (gulp, plugins, paths) {
    return function () {
        return gulp.src(paths.input.admin.sass.dir + paths.input.admin.sass.filename)
            .pipe(plugins.sass().on('error', plugins.sass.logError))
            .pipe(plugins.rename(paths.output.admin.sass.filename))
            .pipe(gulp.dest(paths.output.manifest.cache_dir + paths.output.admin.sass.dir));
    };
};