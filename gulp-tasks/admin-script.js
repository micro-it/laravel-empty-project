module.exports = function (gulp, plugins, paths) {
    return function () {
        return gulp.src(paths.input.admin.script)
            .pipe(plugins.concat(paths.output.admin.script.filename))
            .pipe(gulp.dest(paths.output.manifest.cache_dir + paths.output.admin.script.dir));
    };
};